exports.data = [
	{ username: 'Monster 1', exp: 42 },
	{ username: 'Monster 2', exp: 123 },
	{ username: 'Monster 3', exp: 76 },
	{ username: 'Monster 4', exp: 82 },
	{ username: 'Monster 5', exp: 111 },
	{ username: 'Monster 6', exp: 234 },
	{ username: 'Monster 7', exp: 16 },
	{ username: 'Monster 8', exp: 99 },
];