
var defaultFontSize = Ti.Platform.name === 'android' ? 16 : 14;

var win = Ti.UI.createWindow({
	title: 'Choose a Monster',
	backgroundColor: '#0cf',
});

/*var view = Ti.UI.createView({
	title: 'Choose a Monster',
	backgroundColor: '#0cf',
});
win.add(view);*/
///////////////Buttons////////
/*var screen_buttons = Ti.UI.createView({
	height: 60,
	bottom: 0,
});
win.add(screen_buttons);

var Buttons = require('/view/components/Buttons');
        var buttons = new Buttons();
        screen_buttons.add(buttons.box);*/

// create a controller instance

var Controller = require('Controller');
var controller = new Controller({ win: win });

// set the controller as a global resource
 
Ti.App.controller = controller;

// open the first app screen

Ti.App.controller.show('monsters');

// open the main window

win.open();
