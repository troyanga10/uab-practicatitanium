function ElementPresentation(params) {

    // object properties

    this.box = null;

    // object constructor

    this.init = function() {
        this.build();
    };

    // object functions
    
    this.build = function() {
        var box = Ti.UI.createView({});

			var view1 = Ti.UI.createImageView({ image: '/images/1.png'});
			var view2 = Ti.UI.createImageView({ image: '/images/2.png'});
			var view3 = Ti.UI.createImageView({ image: '/images/3.png'});
			var view4 = Ti.UI.createImageView({ image: '/images/4.png'});
			var view5 = Ti.UI.createImageView({ image: '/images/5.png'});
			var view6 = Ti.UI.createImageView({ image: '/images/6.png'});
			var view7 = Ti.UI.createImageView({ image: '/images/7.png'});
			var view8 = Ti.UI.createImageView({ image: '/images/8.png'});
			
			
			var scrollableView = Ti.UI.createScrollableView({
			  views:[view1,view2,view3,view4,view5,view6,view7,view8],
			  bottom: 40,
			  showPagingControl:true
			});
			box.add(scrollableView);
        
        this.box = box;
    };
    
    this.init();
}

module.exports = ElementPresentation;