function Buttons() {

    // object properties

    this.box = null;

    // object constructor

    this.init = function() {
        this.build();
    };

    // object functions
    
    this.build = function() {
    	
        var buttons = Ti.UI.createView({
        	height: 60,
        });
        
        var button_monster = Ti.UI.createButton({
			title: "Go Monster",
			top: 10,
		    width: 150,
		    height: 50,
		    top: 10,
		    left: 10
		});
		buttons.add(button_monster);

		var button_presentation = Ti.UI.createButton({
			title: "Go Presentation",
			top: 10,
			width: 150,
			height: 50,
			left: 190
		});
		buttons.add(button_presentation);
		
		button_moster.addEventListener('singletap', function(){
        	Ti.App.controller.show('monsters');
        	});
        	
        button_moster.addEventListener('singletap', function(){
        	Ti.App.controller.show('monsters_presentation');
        	});
        	
        this.box = buttons;
    };
    
    this.init();
}

module.exports = Buttons;