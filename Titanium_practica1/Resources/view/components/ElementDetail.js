function ElementDetail(args) {

    // object properties

    this.box = null;

    // object constructor

    this.init = function() {
        this.build();
    };

    // object functions
    
    this.build = function() {
        var box = Ti.UI.createView({});
        
        var monster_id = Ti.App.current_monster_id;
        
        box.add(Ti.UI.createImageView({
            image: '/images/'+8+'.png',
            width: 200,
            height: 200,
            top: 20
        }));
        console.log(monster_id);
        
        box.add(Ti.UI.createLabel({
            text: args.exp,
            top: 300,
            font: { 
                fontSize: defaultFontSize+20, 
                fontWeight: 'bold', 
            },
            textAlign: 'center'
        }));
        
        this.box = box;
    };
    
    this.init();
}

module.exports = ElementDetail;