function Monsters(params) {

    this.box = null;
    
    this.build = function() {
    	
    	

        var box = Ti.UI.createView({
            layout: 'vertical'
        });

        var Header = require('view/components/Header');
        var header = new Header();
        box.add(header.box);

        var monster_data = require('data/monster_data').data;

        var view_table = Ti.UI.createView({
            height:510,
            width:350,
            borderRadius:20,
        });
        box.add(view_table);
        
        var tableData =[]; //require('view/components/ElementList');
		//var defaultFontSize = Ti.Platform.name === 'android' ? 16 : 14;
		var oddRow = false;
		for (var i=0, j=monster_data.length; i<j; i++) {
 		var item = monster_data[i];
 		  
  		var row = Ti.UI.createTableViewRow({
    	height: 80,
    	width:60,
    	
    	backgroundColor: oddRow ? '#fff' : '#eee'
    	
  		});
  		row.add( Ti.UI.createView({
    	backgroundImage: 'images/' +[i+1] + '.png',
    	height: 70,
    	width: 70
  		}));
  		row.add( Ti.UI.createLabel({
    	color: '#576996',
    	font: { fontSize: defaultFontSize+6, fontWeight: 'bold'},
    	text: item.username,
    	left: 20, top: 6,
    	width: 100, height: 30
  		}));
  		
  		row.add( Ti.UI.createLabel({
    	color: '#888',
    	font: { fontSize: defaultFontSize, fontWeight: 'normal'},
    	text: 'power:' + item.exp,
    	left: 20, top: 40,
    	width: 100, height: 20
  		}));
  		tableData.push(row);
  		oddRow = !oddRow;
		}
		var tableView = Ti.UI.createTableView({ data: tableData });
        view_table.add(tableView);
        //Ti.App.current_monster_id = monster_data.indexOf;
        
        view_table.addEventListener('singletap', function(e){
        	Ti.App.controller.show('monster_detail', {id:e.index});
        	
        	
        });
        //Ti.App.current_monster_id = id1;
        var buttons = Ti.UI.createView({
	
		height:60,
		bottom:0,
		color: '#fff',
       	shadowColor: '#444',
        shadowOffset: { x: 1, y: 1 },
        backgroundImage: 'images/headerbg.png',
        backgroundRepeat: true
	
		});
		box.add(buttons);
		
		var monsters_btn = Ti.UI.createButton({
	
		title: 'Home',
		 color: '#fff',
		 font: { fontWeight: 'bold'},
		backgroundColor:'#0cf',
		height:30,
		left: 50,
		borderRadius:10
		});
		
		buttons.add(monsters_btn);
		monsters_btn.addEventListener('singletap', btn_monsters);
		
		var presentation_btn = Ti.UI.createButton({
	
		title: 'Presentation',
		 color: '#fff',
		 font: { fontWeight: 'bold'},
		backgroundColor:'#0cf',
		height:30,
		right: 50,
		borderRadius:10
		
		});
		
		buttons.add(presentation_btn);
		presentation_btn.addEventListener('singletap', btn_present);
		
		
		
		function btn_monsters(){
			
			Ti.App.controller.show('monsters');
		}
		function btn_present(){
			
			Ti.App.controller.show('monster_presentation');
		}
		

        this.box = box;
    };
    
    this.build();
}

module.exports = Monsters;