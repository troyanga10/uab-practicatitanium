function MonsterDetail(params) {

    this.box = null;
    
    this.build = function() {

        var box = Ti.UI.createView({
            
        });
         
        var monster_id = params.id+1;

        var lbl_nombre = Ti.UI.createLabel({	
    	text: 'Monster: '+ monster_id,
    	top:210	
        });
        box.add(lbl_nombre);
        
         var monster_data = require('data/monster_data').data;
         var item = monster_data[monster_id-1];
        
        var lbl_poder = Ti.UI.createLabel({	
    	text: 'Power:'+item.exp,
    	top:230	
        });
        box.add(lbl_poder);
        
        var monster_image = Ti.UI.createImageView({
        	height:100,
        	width:100,
        	top: 100,
        	backgroundImage:'images/'+ monster_id +'.png'
        });
        box.add(monster_image);
       
		var back_btn = Ti.UI.createButton({
	
		title: 'Back',
		color: '#fff',
		font: { fontWeight: 'bold'},
		bottom: 10, 
		width:60, 
		height:60,
		rigth: 15
	
		});
		back_btn.addEventListener('click', btn_back_btn );
		
		function btn_back_btn(){
			
			Ti.App.controller.show('monsters');
		}
		
		box.add(back_btn);
        
        this.box = box;
    };
    
    this.build();
}

module.exports = MonsterDetail;