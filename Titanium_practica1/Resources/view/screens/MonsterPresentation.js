function MonsterPresentation() {

    this.box = null;
    
    this.build = function() {

        var box = Ti.UI.createView({
            layout: 'vertical',
            bottom: 60
        });

        var Header = require('view/components/Header');
        var header = new Header();
        box.add(header.box);

        var monster_data = require('data/monster_data').data;

        	var ElementPresentation = require('view/components/ElementPresentation');
        	for (var i=0, ii=monster_data.length; i<ii; i++) {
        	var el = new ElementPresentation(monster_data[i]);
        	box.add(el.box);
        }

        
        this.box = box;
    };
    
    this.build();
}

module.exports = MonsterPresentation;